package colaAmigos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

/**
 * Representa una cola de personas.
 * 
 * Una cola tiene una lista de personas y las reservas realizadas.
 * 
 * 
 * @author Senmao Ji Ye
 * 
 */
public class Cola {

	private ArrayList<Persona> colaPersonas;
	private ArrayList<Persona[]> colaReservas;

	/**
	 * Crea una cola nueva vacia.
	 * 
	 * 
	 */
	public Cola() {
		colaPersonas = new ArrayList<Persona>();
		colaReservas = new ArrayList<Persona[]>();
	}

	/**
	 * Agrega una persona a la cola y las reservas a la lista de reservas.
	 * 
	 * La persona que agregamos a la lista de amigos no debe estar previamente
	 * en la cola, y tampoco ser una persona nula. Ademas, el numero de reservas
	 * deben ser menor que 11 y ser reservas a amigo, no debe ser un array nulo,
	 * la propia persona no debe estar en las reservas, y no puede reservas 2
	 * veces para un mismo amigo.
	 * 
	 * @param persona
	 *            Persona que se quiere agregar a la cola.
	 * 
	 * @param reserva
	 *            Persona[] - Reservas que solicita la persona.
	 * 
	 * @throws IllegalArgumentException
	 *             Si se intenta agregar a la cola una persona que ya esta en la
	 *             cola o si es una persona nula, y si la lista de reservas es
	 *             nula, el numero de reservas es mayor de 10, reservar a una
	 *             persona que no es amiga, la propia persona no debe estar en
	 *             las reservas, y no puede reservas 2 veces para un mismo
	 *             amigo.
	 */
	public void pedirLaVez(Persona persona, Persona[] reserva) {
		if (persona == null) {
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		}
		if (reserva == null) {
			throw new IllegalArgumentException(
					"Las reservas no pueden ser un valor nulo");
		}
		if (reserva.length > 10) {
			throw new IllegalArgumentException(
					"Las reservas no pueden ser mayor que 10");
		}
		if (colaPersonas.contains(persona)) {
			throw new IllegalArgumentException("La persona ya esta en la cola");
		}
		for (Persona amigo : reserva)
			if (!Arrays.asList(persona.toArrayAmigos()).contains(amigo)) {
				throw new IllegalArgumentException(
						"Las reservas deben de ser a amigos");
			}
		if (Arrays.asList(reserva).contains(persona)) {
			throw new IllegalArgumentException(
					"La reserva no puede contener a la persona que reserva");
		}
		if (new HashSet<Persona>(Arrays.asList(reserva)).size() != Arrays
				.asList(reserva).size()) {
			throw new IllegalArgumentException(
					"La reserva no puede contener a dos personas iguales");
		}

		colaPersonas.add(persona);
		colaReservas.add(reserva);

	}

	/**
	 * Eliminar una persona de la cola y las reservas de la lista de reservas.
	 * 
	 * La persona que eliminamos de la cola es la persona a la que corresponde
	 * el turno. La cola debe contener previamente al menos una persona.
	 * 
	 * 
	 * @throws IllegalArgumentException
	 *             Si se intenta eliminar a alguien de la cola, cuando la cola
	 *             esta vacia.
	 */
	public void atenderPersona() {
		if (colaPersonas.size() == 0) {
			throw new IllegalArgumentException("La persona no esta en la cola");
		}
		colaPersonas.remove(0);
		colaReservas.remove(0);
	}

	/**
	 * Devuelve la primera persona que esta en la cola.
	 * 
	 * La cola debe contener previamente al menos una persona.
	 * 
	 * @return Persona - persona en la primera posicion de la cola.
	 * 
	 * @throws IllegalArgumentException
	 *             Si se realiza la consulta en una cola vacia.
	 */
	public Persona consultaPrimero() {
		if (colaPersonas.size() == 0) {
			throw new IllegalArgumentException("La cola esta vacia");
		}
		return colaPersonas.get(0);
	}

	/**
	 * Devuelve la lista de personas en la cola.
	 *
	 * 
	 * @return Persona[] - lista de personas en la cola.
	 * 
	 */
	public Persona[] toArrayColaPersonas() {
		return colaPersonas.toArray(new Persona[colaPersonas.size()]);
	}

	/**
	 * Devuelve la lista de reservas en la cola.
	 *
	 * 
	 * @return Persona[][] - lista de reservas en la cola.
	 * 
	 */
	public Persona[][] toArrayColaReservas() {
		Persona[][] reserva = new Persona[colaReservas.size()][];
		for (int i = 0; i < colaReservas.size(); i++) {
			reserva[i] = colaReservas.get(i);
		}
		return reserva;
	}

	/**
	 * Devuelve el numero de reservas pendientes de una persona.
	 * 
	 * La cola debe contener previamente al menos una persona, la persona debe
	 * estar en la cola y no ser una persona nula.
	 * 
	 * @param persona
	 *            Persona sobre el que se realiza la consulta.
	 * 
	 * @return int - numero de reservas pendientes de la persona.
	 * 
	 * @throws IllegalArgumentException
	 *             Si se realiza la consulta en una cola vacia, si la persona no
	 *             esta en la cola o es una persona nula.
	 */
	public int reservaPendiente(Persona persona) {
		if (persona == null) {
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		}
		if (!colaPersonas.contains(persona)) {
			throw new IllegalArgumentException("La persona no esta en la cola");
		}
		if (colaPersonas.size() == 0) {
			throw new IllegalArgumentException("La persona no esta en la cola");
		}
		ArrayList<Persona> list = new ArrayList<Persona>(Arrays.asList(colaReservas
				.get(colaPersonas.indexOf(persona))));
		list.removeAll(Collections.singleton(null));
		return list.size();
	}

	/**
	 * Devuelve el numero de reservas iniciales de una persona.
	 * 
	 * La cola debe contener previamente al menos una persona, la persona debe
	 * estar en la cola y no ser una persona nula.
	 * 
	 * @param persona
	 *            Persona sobre el que se realiza la consulta.
	 * 
	 * @return int - numero de reservas iniciales de la persona.
	 * 
	 * @throws IllegalArgumentException
	 *             Si se realiza la consulta en una cola vacia, si la persona no
	 *             esta en la cola o es una persona nula.
	 */
	public int reservaInicial(Persona persona) {
		if (persona == null) {
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		}
		if (!colaPersonas.contains(persona)) {
			throw new IllegalArgumentException("La persona no esta en la cola");
		}
		if (colaPersonas.size() == 0) {
			throw new IllegalArgumentException("La cola esta vacia");
		}

		return colaReservas.get(colaPersonas.indexOf(persona)).length;
	}

	/**
	 * Devuelve la lista de personas que ha colado una persona.
	 * 
	 * La cola debe contener previamente al menos una persona, la persona debe
	 * estar en la cola y no ser una persona nula.
	 * 
	 * @param persona
	 *            Persona sobre el que se realiza la consulta.
	 * 
	 * @return Persona[] - lista de persona que ha colado la persona.
	 * 
	 * @throws IllegalArgumentException
	 *             Si se realiza la consulta en una cola vacia, si la persona no
	 *             esta en la cola o es una persona nula.
	 */
	public Persona[] listaAmigosColados(Persona persona) {
		if (persona == null) {
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		}
		if (!colaPersonas.contains(persona)) {
			throw new IllegalArgumentException("La persona no esta en la cola");
		}
		if (colaPersonas.size() == 0) {
			throw new IllegalArgumentException("La persona no esta en la cola");
		}
		ArrayList<Persona> list = new ArrayList<Persona>(Arrays.asList(colaReservas
				.get(colaPersonas.indexOf(persona))));
		list.removeAll(Collections.singleton(null));
		if (list.size() == colaReservas.get(colaPersonas.indexOf(persona)).length) {
			return new Persona[] {};
		}
		return colaPersonas.subList(colaPersonas.indexOf(persona) - list.size(),
				colaPersonas.indexOf(persona)).toArray(new Persona[list.size()]);

	}

	/**
	 * Agregamos una persona a la cola mediante el sistema de reservas.
	 * 
	 * La consulta se debe realizar en una cola no vacia. La persona debe tener
	 * previamente una reserva realizada por un amigo y que esta sea un amigo, y
	 * no ser una persona nula.
	 * 
	 * @param amigo
	 *            - persona que solicita colarse.
	 * 
	 * @throws IllegalArgumentException
	 *             Si se intenta cola en una cola vacia, si la persona ya esta
	 *             en la cola, o si la persona es nula.
	 */
	public void colarse(Persona amigo) {
		if (amigo == null) {
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		}
		if (colaPersonas.size() == 0) {
			throw new IllegalArgumentException("La cola esta vacia");
		}
		if (colaPersonas.contains(amigo)) {
			throw new IllegalArgumentException(
					"No puedes colar a una persona que ya esta en la cola");
		}

		boolean flag_enReserva = true;
		for (int pos = 0; pos < colaPersonas.size(); pos++) {
			if (Arrays.asList(amigo.toArrayAmigos()).contains(colaPersonas.get(pos))) {
				if (Arrays.asList(colaReservas.get(pos)).contains(amigo)) {
					colaReservas.get(pos)[Arrays.asList(colaReservas.get(pos)).indexOf(
							amigo)] = null;
					colaPersonas.add(pos, amigo);
					colaReservas.add(pos, new Persona[] {});
					flag_enReserva = false;
				}
			}
		}
		if (flag_enReserva) {
			throw new IllegalArgumentException("La persona no tiene reserva");
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colaPersonas == null) ? 0 : colaPersonas.hashCode());
		result = prime * result
				+ ((colaReservas == null) ? 0 : colaReservas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cola other = (Cola) obj;
		if (colaPersonas == null) {
			if (other.colaPersonas != null)
				return false;
		} else if (!colaPersonas.equals(other.colaPersonas))
			return false;
		if (colaReservas == null) {
			if (other.colaReservas != null)
				return false;
		} else if (!colaReservas.equals(other.colaReservas))
			return false;
		return true;
	}
}
