package colaAmigos;

import java.util.ArrayList;

/**
 * Representa una persona.
 * 
 * Una persona tiene un nombre, ademas de una lista de conocidos y una lista de
 * amigos con las se relaciona.
 * 
 * 
 * 
 * @author Senmao Ji Ye
 * 
 */
public class Persona {

	private String nombre;
	private ArrayList<Persona> listaAmigos;
	private ArrayList<Persona> listaConocidos;

	/**
	 * Crea una persona con un nombre asociado.
	 * 
	 * El nombre no puede estar vacio o ser nulo.
	 * 
	 * @param nombre
	 *            Nombre que tendra la persona.
	 * 
	 * @throws IllegalArgumentException
	 *             Si se intenta crear una persona sin nombre o con nombre nulo.
	 */
	public Persona(String nombre) {
		if (nombre == null) {
			throw new IllegalArgumentException(
					"El nombre no puede ser un valor nulo");
		}
		if (nombre == "") {
			throw new IllegalArgumentException(
					"El nombre debe contener algun caracter");
		}
		this.nombre = nombre;
		listaAmigos = new ArrayList<Persona>();
		listaConocidos = new ArrayList<Persona>();
	}

	/**
	 * Agrega una persona a la lista de conocidos.
	 * 
	 * La persona que agregamos a la lista de conocidos no debe estar
	 * previamente en la lista de amigos o en la lista de conocidos, ni tampoco
	 * ser la propia persona o ser una persona nula.
	 * 
	 * @param conocido
	 *            Persona que se quiere agregar a la lista de conocidos.
	 * 
	 * @throws IllegalArgumentException
	 *             Si se intenta agregar a la lista de conocidos una persona que
	 *             ya esta en la lista de conocidos o en la lista de amigos, o
	 *             cuando la persona es uno mismo o una persona nula.
	 */
	public void addConocido(Persona conocido) {
		if (conocido == null) {
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		}
		if (this.equals(conocido)) {
			throw new IllegalArgumentException(
					"No puedes ser conocido de uno mismo");
		}
		if (listaConocidos.contains(conocido)) {
			throw new IllegalArgumentException("La persona ya es conocida");
		}
		if (listaAmigos.contains(conocido)) {
			throw new IllegalArgumentException("La persona ya es amiga");
		}
		listaConocidos.add(conocido);

	}

	/**
	 * Agrega una persona a la lista de amigos.
	 * 
	 * La persona que agregamos a la lista de amigos no debe estar previamente
	 * en la lista de amigos o en la lista de conocidos, ni tampoco ser la
	 * propia persona o ser una persona nula.
	 * 
	 * @param amigo
	 *            Persona que se quiere agregar a la lista de amigos.
	 * 
	 * @throws IllegalArgumentException
	 *             Si se intenta agregar a la lista de amigos una persona que ya
	 *             esta en la lista de conocidos o en la lista de amigos, o
	 *             cuando la persona es uno mismo o una persona nula.
	 */
	public void addAmigo(Persona amigo) {
		if (amigo == null) {
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		}
		if (this.equals(amigo)) {
			throw new IllegalArgumentException(
					"No puedes ser amigo de uno mismo");
		}
		if (listaAmigos.contains(amigo)) {
			throw new IllegalArgumentException("La persona ya es amiga");
		}
		if (!listaConocidos.contains(amigo)) {
			throw new IllegalArgumentException("La persona no es conocida");
		}
		listaConocidos.remove(amigo);
		listaAmigos.add(amigo);

	}

	/**
	 * Eliminar una persona de la lista de amigos.
	 * 
	 * La persona que eliminamos a la lista de amigos debe estar previamente en
	 * la lista de amigos, no ser una persona nula o ser la propia persona.
	 * 
	 * @param amigo
	 *            Persona que se quiere eliminar a la lista de amigos.
	 * 
	 * @throws IllegalArgumentException
	 *             Si se intenta eliminar de la lista de amigos una persona que
	 *             no esta en la lista de amigos, es una persona nula o es la
	 *             propia persona
	 */
	public void removeAmigo(Persona amigo) {
		if (amigo == null) {
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		}
		if (this.equals(amigo)) {
			throw new IllegalArgumentException(
					"No puedes eliminarse a uno mismo");
		}
		if (listaConocidos.contains(amigo)) {
			throw new IllegalArgumentException("La persona es un conocido");
		}
		if (!listaAmigos.contains(amigo)) {
			throw new IllegalArgumentException("La persona no es conocida");
		}
		listaAmigos.remove(amigo);
		listaConocidos.add(amigo);
	}

	/**
	 * Devuelve un array de personas que son amigos.
	 * 
	 * @return Persona[] - Array de amigos
	 */
	public Persona[] toArrayAmigos() {
		return listaAmigos.toArray(new Persona[listaAmigos.size()]);
	}

	/**
	 * Devuelve un array de personas que son conocidos.
	 * 
	 * @return Persona[] - Array de conocidos.
	 */
	public Persona[] toArrayConocidos() {
		return listaConocidos.toArray(new Persona[listaConocidos.size()]);
	}

	/**
	 * Devuelve el nombre de la persona.
	 * 
	 * @return String - Nombre de la persona.
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Indica si una persona es amiga.
	 * 
	 * La persona que queremos consultar si es amiga, de esta en la lista de
	 * amigos, no ser una persona nula o ser la propia persona.
	 * 
	 * @param persona
	 *            Persona que se quiere consultar si esta en la lista de amigos.
	 * 
	 * @return boolean - true si la persona esta en la lista de amigos, false en
	 *         caso contrario.
	 * 
	 * @throws IllegalArgumentException
	 *             Si es una persona nula o es la misma persona.
	 */
	public boolean esAmigo(Persona persona) {
		if (persona == null) {
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		}
		if (this.equals(persona)) {
			throw new IllegalArgumentException(
					"La persona no puede ser uno mismo");
		}
		if (listaAmigos.contains(persona))
			return true;
		else
			return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((listaAmigos == null) ? 0 : listaAmigos.hashCode());
		result = prime * result
				+ ((listaConocidos == null) ? 0 : listaConocidos.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (listaConocidos == null) {
			if (other.listaConocidos != null)
				return false;
		} else if (!listaConocidos.equals(other.listaConocidos))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}
