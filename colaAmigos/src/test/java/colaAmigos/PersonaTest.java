package colaAmigos;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PersonaTest {

	private Persona persona;
	private Persona primer_amigo;
	private Persona primer_conocido;

	@Before
	public void setUp() {
		persona = new Persona("nombre");
		primer_amigo = new Persona("primer_amigo");
		primer_conocido = new Persona("`rimero_conocido");
		persona.addConocido(primer_amigo);
		persona.addConocido(primer_conocido);
		persona.addAmigo(primer_amigo);
	}

	@After
	public void tearDown() {
		persona = null;
	}

	@Test
	public void testCrearPersona() {
		Persona amigo = new Persona("nombre");
		assertEquals("nombre", amigo.getNombre());
		assertArrayEquals(new Persona[] {}, amigo.toArrayConocidos());
		assertArrayEquals(new Persona[] {}, amigo.toArrayAmigos());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearPersonaNombreNull() {
		Persona amigo = new Persona(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrearPersonaNombreVacio() {
		Persona amigo = new Persona("");
	}

	@Test
	public void testAgregarConocido() {
		Persona conocido = new Persona("nuevo conocido");
		persona.addConocido(conocido);
		Persona[] personasObtenidas = persona.toArrayConocidos();
		Persona[] personasEsperadas = { primer_conocido, conocido };
		assertArrayEquals(personasObtenidas, personasEsperadas);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarConocidoNull() {
		persona.addConocido(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarConocidoRepetido() {
		persona.addConocido(primer_conocido);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarConocidoUnoMismo() {
		persona.addConocido(persona);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarConocidoAmigo() {
		Persona conocido = new Persona("conocido");
		persona.addConocido(conocido);
		persona.addAmigo(conocido);
		persona.addConocido(conocido);
	}

	@Test
	public void testAgregarAmigo() {
		persona.addAmigo(primer_conocido);
		Persona[] personasObtenidas = persona.toArrayAmigos();
		Persona[] personasEsperadas = { primer_amigo, primer_conocido };
		assertArrayEquals(personasObtenidas, personasEsperadas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarAmigoNull() {
		persona.addAmigo(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarAmigoRepetido() {
		persona.addAmigo(primer_amigo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarAmigoUnoMismo() {
		persona.addAmigo(persona);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarAmigoDesconocido() {
		Persona desconocido = new Persona("desconocido");
		persona.addAmigo(desconocido);
	}

	@Test
	public void testEliminarAmigo() {
		persona.removeAmigo(primer_amigo);
		Persona[] amigosObtenidos = persona.toArrayAmigos();
		Persona[] amigosEsperados = new Persona[] {};
		assertArrayEquals(amigosObtenidos, amigosEsperados);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarAmigoNull() {
		persona.removeAmigo(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarAmigoDesconocido() {
		Persona desconocido = new Persona("desconocido");
		persona.removeAmigo(desconocido);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarAmigoUnoMismo() {
		persona.removeAmigo(persona);
	}

	@Test
	public void testConsultaAmigo() {
		boolean esAmigo = persona.esAmigo(primer_amigo);
		assertTrue(esAmigo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaAmigoNull() {
		persona.esAmigo(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaAmigoUnoMismo() {
		persona.esAmigo(persona);
	}

	@Test
	public void testObtenerArrayAmigo() {
		Persona[] amigosObtenidos = persona.toArrayAmigos();
		Persona[] amigosEsperados = { primer_amigo };
		assertArrayEquals(amigosObtenidos, amigosEsperados);
	}

}
