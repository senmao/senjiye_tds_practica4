package colaAmigos;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.easymock.Mock;
import static org.easymock.EasyMock.*;

public class ColaMockTest {

	private Cola colaAmigos;
	@Mock
	private Persona persona;
	@Mock
	private Persona amigo;
	@Mock
	private Persona desconocido;
	private Persona[] reservas;

	@Before
	public void setUp() {
		persona = createMock(Persona.class);
		amigo = createMock(Persona.class);
		desconocido = createMock(Persona.class);
		expect(persona.getNombre()).andReturn("Persona").anyTimes();
		expect(amigo.getNombre()).andReturn("Amigo").anyTimes();
		expect(desconocido.getNombre()).andReturn("Desconocido").anyTimes();

		expect(persona.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.anyTimes();
		expect(amigo.toArrayAmigos()).andReturn(new Persona[] { persona })
				.anyTimes();
		expect(desconocido.toArrayAmigos()).andReturn(new Persona[] {})
				.anyTimes();
		expect(persona.toArrayConocidos()).andReturn(new Persona[] {})
				.anyTimes();
		expect(amigo.toArrayConocidos()).andReturn(new Persona[] {}).anyTimes();
		expect(desconocido.toArrayConocidos()).andReturn(new Persona[] {})
				.anyTimes();
		replay(persona);
		replay(amigo);
		replay(desconocido);

		colaAmigos = new Cola();
		reservas = new Persona[] { amigo };

	}

	@After
	public void tearDown() {
		colaAmigos = null;
	}

	@Test
	public void testCrearCola() {
		Cola colaAmigos = new Cola();
		Persona[] valorObtenido = colaAmigos.toArrayColaPersonas();
		Persona[] valorEsperado = new Persona[] {};
		assertArrayEquals(valorObtenido, valorEsperado);
	}

	@Test
	public void testAgregarCola() {
		colaAmigos.pedirLaVez(persona, reservas);
		Persona[] valorObtenido = colaAmigos.toArrayColaPersonas();
		Persona[] valorEsperado = new Persona[] { persona };
		Persona[][] reservasObtenido = colaAmigos.toArrayColaReservas();
		Persona[][] reservasEsperado = new Persona[][] { { amigo } };
		assertArrayEquals(valorObtenido, valorEsperado);
		assertArrayEquals(reservasObtenido, reservasEsperado);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaPersonaNull() {
		colaAmigos.pedirLaVez(null, reservas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaReservasNull() {
		colaAmigos.pedirLaVez(persona, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaReservasOutBound() {
		Persona amigo = createMock(Persona.class);
		Persona amigo1 = createMock(Persona.class);
		Persona amigo2 = createMock(Persona.class);
		Persona amigo3 = createMock(Persona.class);
		Persona amigo4 = createMock(Persona.class);
		Persona amigo5 = createMock(Persona.class);
		Persona amigo6 = createMock(Persona.class);
		Persona amigo7 = createMock(Persona.class);
		Persona amigo8 = createMock(Persona.class);
		Persona amigo9 = createMock(Persona.class);
		Persona amigo10 = createMock(Persona.class);
		Persona amigo11 = createMock(Persona.class);
		expect(amigo.toArrayAmigos()).andReturn(
				new Persona[] { amigo1, amigo2, amigo3, amigo4, amigo5, amigo6,
						amigo7, amigo, amigo9, amigo10, amigo11 }).once();
		expect(amigo1.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		expect(amigo2.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		expect(amigo3.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		expect(amigo4.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		expect(amigo5.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		expect(amigo6.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		expect(amigo7.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		expect(amigo8.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		expect(amigo9.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		expect(amigo10.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		expect(amigo11.toArrayAmigos()).andReturn(new Persona[] { amigo })
				.once();
		replay(amigo);
		replay(amigo1);
		replay(amigo2);
		replay(amigo3);
		replay(amigo4);
		replay(amigo5);
		replay(amigo6);
		replay(amigo7);
		replay(amigo8);
		replay(amigo9);
		replay(amigo10);
		replay(amigo11);

		Persona[] reservas = new Persona[] { amigo1, amigo2, amigo3, amigo4,
				amigo, amigo6, amigo7, amigo8, amigo9, amigo10, amigo11 };
		colaAmigos.pedirLaVez(persona, reservas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaReservasNoAmigos() {
		Persona[] reservas = new Persona[] { desconocido };
		colaAmigos.pedirLaVez(persona, reservas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaReservasUnoMismo() {
		Persona[] reservas = new Persona[] { persona };
		colaAmigos.pedirLaVez(persona, reservas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaReservasAmigoRepetido() {
		Persona[] reservas = new Persona[] { amigo, amigo };
		colaAmigos.pedirLaVez(persona, reservas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarPersonaRepetida() {
		colaAmigos.pedirLaVez(persona, reservas);
		colaAmigos.pedirLaVez(persona, reservas);
	}

	@Test
	public void testColarse() {
		colaAmigos.pedirLaVez(persona, reservas);
		colaAmigos.colarse(amigo);
		Persona[] personasObtenidas = colaAmigos.toArrayColaPersonas();
		Persona[] personasEsperadas = new Persona[] { amigo, persona };
		assertArrayEquals(personasObtenidas, personasEsperadas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testColarseEstaEnCola() {
		colaAmigos.pedirLaVez(persona, reservas);
		colaAmigos.colarse(persona);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testColarseSinReserva() {
		colaAmigos.pedirLaVez(persona, reservas);
		colaAmigos.colarse(desconocido);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testColarseColaVacia() {
		colaAmigos.colarse(desconocido);
	}

	@Test
	public void testEliminarCola() {
		colaAmigos.pedirLaVez(persona, reservas);
		colaAmigos.atenderPersona();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarColaVacia() {
		colaAmigos.atenderPersona();
	}

	@Test
	public void testConsultaReservaInicial() {
		colaAmigos.pedirLaVez(persona, reservas);
		int valorObtenido = colaAmigos.reservaInicial(persona);
		int valorEsperado = 1;
		assertEquals(valorObtenido, valorEsperado);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaInicialNull() {
		int valorObtenido = colaAmigos.reservaInicial(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaInicialDesconocido() {
		colaAmigos.pedirLaVez(persona, reservas);
		int valorObtenido = colaAmigos.reservaInicial(desconocido);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaInicialColaVacia() {
		int valorObtenido = colaAmigos.reservaInicial(persona);
	}

	@Test
	public void testConsultaReservaPendiente() {
		colaAmigos.pedirLaVez(persona, reservas);
		int valorObtenido = colaAmigos.reservaPendiente(persona);
		int valorEsperado = 1;
		assertEquals(valorObtenido, valorEsperado);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaPendienteNull() {
		int valorObtenido = colaAmigos.reservaPendiente(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaPendienteDesconocido() {
		int valorObtenido = colaAmigos.reservaPendiente(desconocido);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaPendienteColaVacia() {
		int valorObtenido = colaAmigos.reservaPendiente(persona);
	}

	@Test
	public void testConsultaPrimero() {
		colaAmigos.pedirLaVez(persona, reservas);
		Persona personaObtenida = colaAmigos.consultaPrimero();
		Persona personaEsperada = persona;
		assertEquals(personaObtenida, personaEsperada);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaPrimeroColaVacia() {
		Cola colaNueva = new Cola();
		Persona personaObtenida = colaNueva.consultaPrimero();
	}

	@Test
	public void testConsultaAmigosColados() {
		colaAmigos.pedirLaVez(persona, reservas);
		Persona[] personasObtenida = colaAmigos.listaAmigosColados(persona);
		Persona[] personasEsperada = new Persona[] {};
		assertArrayEquals(personasObtenida, personasEsperada);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaAmigosColadosNull() {
		colaAmigos.pedirLaVez(persona, reservas);
		Persona[] personasObtenida = colaAmigos.listaAmigosColados(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaAmigosColadosColaVacia() {
		Persona[] personasObtenida = colaAmigos.listaAmigosColados(persona);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaAmigosColadosDesconocido() {
		colaAmigos.pedirLaVez(persona, reservas);
		Persona[] personasObtenida = colaAmigos.listaAmigosColados(new Persona(
				"Desconocido"));
	}

}
