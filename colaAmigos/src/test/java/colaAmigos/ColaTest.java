package colaAmigos;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ColaTest {

	private Cola colaAmigos;
	private Persona persona, amigo, conocido;
	private Persona[] reservas;

	@Before
	public void setUp() {
		// Creamos una cola valida para la prueba de metodos.
		ArrayList<Persona> cola = new ArrayList<Persona>();
		persona = new Persona("persona");
		amigo = new Persona("persona_1");
		conocido = new Persona("persona_2");
		persona.addConocido(amigo);
		persona.addConocido(conocido);
		persona.addAmigo(amigo);
		reservas = new Persona[] { amigo };
		colaAmigos = new Cola();
	}

	@After
	public void tearDown() {
		colaAmigos = null;
	}

	@Test
	public void testCrearCola() {
		Cola colaAmigos = new Cola();
		Persona[] valorObtenido = colaAmigos.toArrayColaPersonas();
		Persona[] valorEsperado = new Persona[] {};
		assertArrayEquals(valorObtenido, valorEsperado);
	}

	@Test
	public void testAgregarCola() {
		colaAmigos.pedirLaVez(persona, reservas);
		Persona[] valorObtenido = colaAmigos.toArrayColaPersonas();
		Persona[] valorEsperado = new Persona[] { persona };
		Persona[][] reservasObtenido = colaAmigos.toArrayColaReservas();
		Persona[][] reservasEsperado = new Persona[][] { { persona } };
		assertArrayEquals(valorObtenido, valorEsperado);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaPersonaNull() {
		colaAmigos.pedirLaVez(null, reservas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaReservasNull() {
		colaAmigos.pedirLaVez(persona, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaReservasOutBound() {
		Persona[] reservas = new Persona[] { new Persona("amigo1"),
				new Persona("amigo2"), new Persona("amigo3"),
				new Persona("amigo4"), new Persona("amigo5"),
				new Persona("amigo6"), new Persona("amigo7"),
				new Persona("amigo8"), new Persona("amigo9"),
				new Persona("amigo10"), new Persona("amigo11") };
		colaAmigos.pedirLaVez(persona, reservas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaReservasNoAmigos() {
		Persona[] reservas = new Persona[] { new Persona("desconocido") };
		colaAmigos.pedirLaVez(persona, reservas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaReservasUnoMismo() {
		Persona[] reservas = new Persona[] { persona };
		colaAmigos.pedirLaVez(persona, reservas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarColaReservasAmigoRepetido() {
		Persona[] reservas = new Persona[] { amigo, amigo };
		colaAmigos.pedirLaVez(persona, reservas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarPersonaRepetida() {
		colaAmigos.pedirLaVez(persona, reservas);
		colaAmigos.pedirLaVez(persona, reservas);
	}

	@Test
	public void testColarse() {
		colaAmigos.pedirLaVez(persona, reservas);
		amigo.addConocido(persona);
		amigo.addAmigo(persona);
		colaAmigos.colarse(amigo);
		Persona[] personasObtenidas = colaAmigos.toArrayColaPersonas();
		Persona[] personasEsperadas = new Persona[] { amigo, persona };
		assertArrayEquals(personasObtenidas, personasEsperadas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testColarseEstaEnCola() {
		colaAmigos.pedirLaVez(persona, reservas);
		colaAmigos.colarse(persona);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testColarseSinReserva() {
		colaAmigos.pedirLaVez(persona, reservas);
		colaAmigos.colarse(conocido);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testColarseColaVacia() {
		colaAmigos.colarse(conocido);
	}

	@Test
	public void testEliminarCola() {
		colaAmigos.pedirLaVez(persona, reservas);
		colaAmigos.atenderPersona();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEliminarColaVacia() {
		colaAmigos.atenderPersona();
	}

	@Test
	public void testConsultaReservaInicial() {
		colaAmigos.pedirLaVez(persona, reservas);
		int valorObtenido = colaAmigos.reservaInicial(persona);
		int valorEsperado = 1;
		assertEquals(valorObtenido, valorEsperado);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaInicialNull() {
		int valorObtenido = colaAmigos.reservaInicial(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaInicialDesconocido() {
		colaAmigos.pedirLaVez(persona, reservas);
		int valorObtenido = colaAmigos
				.reservaInicial(new Persona("Desconocido"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaInicialColaVacia() {
		int valorObtenido = colaAmigos.reservaInicial(persona);
	}

	@Test
	public void testConsultaReservaPendiente() {
		colaAmigos.pedirLaVez(persona, reservas);
		int valorObtenido = colaAmigos.reservaPendiente(persona);
		int valorEsperado = 1;
		assertEquals(valorObtenido, valorEsperado);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaPendienteNull() {
		int valorObtenido = colaAmigos.reservaPendiente(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaPendienteDesconocido() {
		int valorObtenido = colaAmigos.reservaPendiente(new Persona(
				"Desconocido"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaReservaPendienteColaVacia() {
		int valorObtenido = colaAmigos.reservaPendiente(persona);
	}

	@Test
	public void testConsultaPrimero() {
		colaAmigos.pedirLaVez(persona, reservas);
		Persona personaObtenida = colaAmigos.consultaPrimero();
		Persona personaEsperada = persona;
		assertEquals(personaObtenida, personaEsperada);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaPrimeroColaVacia() {
		Cola colaNueva = new Cola();
		Persona personaObtenida = colaNueva.consultaPrimero();
	}

	@Test
	public void testConsultaAmigosColados() {
		colaAmigos.pedirLaVez(persona, reservas);
		Persona[] personasObtenida = colaAmigos.listaAmigosColados(persona);
		Persona[] personasEsperada = new Persona[] {};
		assertArrayEquals(personasObtenida, personasEsperada);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaAmigosColadosNull() {
		colaAmigos.pedirLaVez(persona, reservas);
		Persona[] personasObtenida = colaAmigos.listaAmigosColados(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaAmigosColadosColaVacia() {
		Persona[] personasObtenida = colaAmigos.listaAmigosColados(persona);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConsultaAmigosColadosDesconocido() {
		colaAmigos.pedirLaVez(persona, reservas);
		Persona[] personasObtenida = colaAmigos.listaAmigosColados(new Persona(
				"Desconocido"));
	}

}
