# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

El repositorio dispone de un proyecto maven de Eclipse, donde disponemos de las clases para simular una Cola con un sistema de reservas, y las personas que acceden a ella.
Dispone también del un script ant xml para automatizar las pruebas.
* Version

La versión del proyecto es la snapshot 0.0.1.

* Author notes

-En la conceptualizacion del proyecto, una persona considerar que una persona es su amiga, sin que esta la considere su amigo.
-Para consultar el historial de refactoring, se dispone la carpeta de .refactoring, con los archivos generados por el IDE eclipse.


### How do I get set up? ###

* Summary of set up

Las clases Java donde se implementan toda la funcionalidad se encuentran disponibles en la carpeta *colaAmigos/src/main/java*.
También se disponen de los test, implementados con clases junit, de estas clases en *colaAmigos/src/test/java*.

* Configuration

El proyecto requiere de [Java 7](http://www.oracle.com/technetwork/es/java/javase/downloads/jdk7-downloads-1880260.html) para poder ejecutarse sin ningun problema de compatibilidad.
Para cargar el proyecto en un proyecto eclipse, es necesario que se compile previamente con ant script, disponible en el propio IDE de eclipse.

* Dependencies

El directorio lib contiene las dependencias del proyecto.
En ella se encuentra junit 4.11 y otras dependencias como EasyMock, necesaria para el funcionamiento de los MockObject en las pruebas de caja blanca. 

* How to run tests

El proyecto dispone de una Junit Test Suite, AllTest.java, que contiene todos los tests relacionadas con las dos clases que disponemos, una prueba de caja negra 
para Persona y otra para Cola, y una prueba de caja blanca para la clase Cola. Todas ellas se encuentran dentro de *colaAmigos/src/test/java* 

* Deployment instructions

Es necesario ejecutar el script ant build.xml, ya sea desde consola o desde el IDE de eclipse.
Desde consola, partirendo desde la raiz del proyecto, disponemos de los siguientes comandos:

```ant compilar``` **(Default)** garantiza la obtencion de todas las dependencias y genera los .class a partir de los fuentes

```ant init``` generamos las carpetas que contendrán los archivos .class generados durante la compilacion

```ant limpiar``` para borrar los archivos .class y las carpetas creadas para su almacenamiento.

```ant ejecutar``` ejecuta las pruebas de caja negra y caja blanca.

```ant ejecutarTestEnAislamiento``` ejecuta las pruebas de caja blanca.

```ant ejecutarTestsTDD``` ejecuta las pruebas de caja negra.

```ant obtenerInformeCobertura``` genera un informe de la cobertura del las fuentes de proyecto, almacenándola en el directorio *colaAmigos/target/site/cobertura*.

```ant documentar``` genera la carpeta doc con la documentacion autocontenida en los fuentes mediante javadoc, almacenándola en la carpeta *colaAmigos/doc*.


### Contribution guidelines ###

* Writing tests

Todos los tests han sido escritos por el administrador del repositorio: Senmao Ji Ye (Username:Senmao)

### Who do I talk to? ###

* Repo owner or admin

Creador del repositorio: Senmao Ji Ye (Username: Senmao)